# Curso: Lenguaje de programación JavaScript

## Práctica Final: JQuery, Ajax, DOM, JSON

Modificar los archivos buscar.html y js/buscar.js para dar funcionalidad a la interfaz de búsqueda.
Se trata de buscar coincidencias usando el API de www.themoviedb.org, según la palabra indicada por el usuario en la caja 
de búsqueda y mostrar los resultados en una lista dentro de la misma página (no se tienen que mostrar todos los atributos 
de la película, será suficiente con que se muestren los títulos o los que tú elijas).

## Resolución

+ La parte de Ajax se reutilizo del archivo index.js al archivo buscar.js
+ La URL de la api se concatena con la palabra ingresada en el input buscar "query =+ palabra"
+ Unicamente se muestra Imagen y Titulo de la pelicula omitiendo los demás datos como la descripción